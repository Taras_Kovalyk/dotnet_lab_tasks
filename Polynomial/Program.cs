﻿using System;


namespace Polynomial
{
    class Program
    {
        static void Main()
        {
            double[] aCoefs = { -5, -2, 6.4, 1 };
            double[] bCoefs = { -1, 3, 4.2, 1 };

            var a = new Polynomial(3, aCoefs);
            var b = new Polynomial(3, bCoefs);

            Console.WriteLine("a = " + a);
            Console.WriteLine("b = " + b);
            Console.WriteLine("\n" + new string('-',50)+"\n");

            Console.WriteLine("a + b = " + (a + b));
            Console.WriteLine();

            Console.WriteLine("a - b = " + (a - b));
            Console.WriteLine();

            Console.WriteLine("a * b = " + (a * b));
            Console.WriteLine();

            Console.WriteLine("\n" + new string('-', 50) + "\n");

            Console.WriteLine("Calculate a(10): " + a.Calculate(10));
            Console.WriteLine();

            Console.Read();
        }
    }
}
