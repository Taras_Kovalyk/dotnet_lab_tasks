﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Matrix
{
    public class Matrix
    {
        private double[,] _matrix;


        #region Constructors

        public Matrix() { }


        public Matrix(int height, int width)
        {
            _matrix = new double[height, width];
        }


        public Matrix(int[,] matrix)
        {
            InitMatrix(matrix);
        }


        public Matrix(float[,] matrix)
        {
            InitMatrix(matrix);
        }


        public Matrix(double[,] matrix)
        {
            _matrix = (double[,])matrix.Clone();
        }
        #endregion

       
        #region Properties

        public int RowCount => _matrix.GetLength(0);

        public int ColumnCount => _matrix.GetLength(1);


        public double this[int i, int j]
        {
            get
            {
                if (i >= 0 && i < RowCount && j >= 0 && j < ColumnCount)
                {
                    return _matrix[i, j];
                }
                 
                throw new IndexOutOfRangeException();
            }

            set
            {
                if ((i >= 0 && i < RowCount) && (j >= 0 && j < ColumnCount))
                {
                    _matrix[i, j] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }
        #endregion


        #region Operators

        public static Matrix operator *(Matrix a, Matrix b)
        {
            if (a.RowCount != b.ColumnCount)
            {
                throw new ArgumentException("To multiply  matrixes, the number of rows in the first," +
                                            " must be equel to the numder of columns in the second");
            }

            var resultMatrix = new Matrix(a.RowCount, b.ColumnCount);

                for (var i = 0; i < resultMatrix.RowCount; i++)
                {
                    for (var j = 0; j < resultMatrix.ColumnCount; j++)
                    {
                        for (var k = 0; k < b.RowCount; k++)
                        {
                            resultMatrix[i, j] += a[i, k] * b[k, j];
                        }
                    }
                }
                return resultMatrix;
            
        }


        public static Matrix operator +(Matrix a, Matrix b)
        {
            if (a.RowCount != b.RowCount ||  a.ColumnCount != b.ColumnCount)
            {
                throw new ArgumentException("To add two matrixes, they must be the same size.");
            }

            var resultMatrix = new Matrix(a.RowCount, a.ColumnCount);

                for (var i = 0; i < resultMatrix.RowCount; i++)
                {
                    for (var j = 0; j < resultMatrix.ColumnCount; j++)
                    {
                            resultMatrix[i, j] = a[i, j] + b[i, j];
                    }
                }
                return resultMatrix;       
        }

        public static Matrix operator -(Matrix a, Matrix b)
        {
            if (a.RowCount != b.RowCount || a.ColumnCount != b.ColumnCount)
            {
                throw new ArgumentException("To add two matrixes, they must be the same size.");
            }

            var resultMatrix = new Matrix(a.RowCount, a.ColumnCount);

                for (var i = 0; i < resultMatrix.RowCount; i++)
                {
                    for (var j = 0; j < resultMatrix.ColumnCount; j++)
                    {
                        resultMatrix[i, j] = a[i, j] - b[i, j];
                    }
                }
                return resultMatrix;

        }


        #endregion


        #region Methods

        public void InitMatrix<T>(T[,] matrix) where T : IConvertible
        {
            _matrix = new double[matrix.GetLength(0), matrix.GetLength(1)];

            for (var i = 0; i < RowCount; i++)
            {
                for (var j = 0; j < ColumnCount; j++)
                {
                    _matrix[i, j] = Convert.ToDouble(matrix[i, j]);
                }
            }
        }


        /// <summary>
        /// Calculates minor of (i,j) element for current mutrix
        /// </summary>
        public double GetMinor(int i, int j)
        {
            if (i <= 0 || i >= RowCount || j <= 0 || j >= ColumnCount)
            {
                throw new ArgumentException("Element is out of matrix range");
            }

            var minor = new double[RowCount - 1, ColumnCount - 1];

            for (int rowMatrix = 0, rowMinor = 0; rowMatrix < RowCount; rowMatrix++, rowMinor++)
            {
                if (rowMatrix != i)
                {
                    for (int colMatrix = 0, colMinor = 0; colMatrix < ColumnCount; colMatrix++, colMinor++)
                    {
                        if (colMatrix != j)
                        {
                            minor[rowMinor, colMinor] = _matrix[rowMatrix, colMatrix];
                        }
                        else
                        {
                            colMinor--;
                        }
                    }
                }
                else
                {
                    rowMinor--;
                }
            }

            return GetDeterminant(minor);
        }

        /// <summary>
        /// Calculates minor of k-th order for current matrix
        /// </summary>
        /// <param name="rows">Contains selected indexes of rows</param>
        /// <param name="cols">>Contains selected indexes of columns</param>
        /// <returns></returns>
        public double GetMinorOfKthOrder(int[] rows, int[] cols)
        {
            if (rows.Length != cols.Length)
            {
                throw new ArgumentException("To calculate minor of k-th order, number of rows and columns must be the same");
            }

            var minor = new double[rows.Length, cols.Length];

            for (var i = 0; i < rows.Length; i++)
            {
                for (var j = 0; j < cols.Length; j++)
                {
                    minor[i, j] = _matrix[rows[i], cols[j]];
                }
            }

            return GetDeterminant(minor);
        }

        /// <summary>
        /// Calculates additional minor of current matrix
        /// </summary>
        /// <param name="rows">Contains selected indexes of rows</param>
        /// <param name="cols">>Contains selected indexes of columns</param>
        /// <returns></returns>
        public double GetAdditionalMinor(int[] rows, int[] cols)
        {
            if (rows.Length != cols.Length)
            {
                throw new ArgumentException("To calculate additional minor, number of rows and columns must be the same");
            }

            double[,] minor = new double[RowCount - rows.Length, ColumnCount - cols.Length];

            for (int i = 0, row = 0; i < RowCount; i++, row++)
            {
                if (!rows.Contains(i))
                {
                    for (int j = 0, col = 0; j < ColumnCount; j++, col++)
                    {
                        if (!cols.Contains(j))
                        {
                            minor[row, col] = _matrix[i, j];
                        }
                        else
                        {
                            col--;
                        }
                    }
                }
                else
                {
                    row--;
                }
            }

            return GetDeterminant(minor);
        }



        public double GetDeterminant()
        {
            return GetDeterminant(_matrix);
        }

        private double GetDeterminant(double[,] matrix)
        {
            double det;
            int n = matrix.GetLength(0);
            var a = (double[,])matrix.Clone();

            for (var i = 0; i < n - 1; i++)
            {
                for (var j = i + 1; j < n; j++)
                {
                    det = a[j, i] / a[i, i];
                    for (var k = i; k < n; k++)
                    {
                        a[j, k] -= det * a[i, k];
                    }
                }
            }

            det = 1;
            for (var i = 0; i < n; i++)
                det *= a[i, i];

            return det;
            
        }


        public override string ToString()
        {
            StringBuilder outputMatrix = new StringBuilder();
            for (var i = 0; i < RowCount; i++)
            {
                outputMatrix.Append("|");

                for (var j = 0; j < ColumnCount; j++)
                {
                    outputMatrix.Append(_matrix[i, j].ToString(CultureInfo.CurrentCulture).PadLeft(10));
                }

                outputMatrix.Append("\t|");
                outputMatrix.AppendLine();
            }

            return outputMatrix.ToString();
        }

        #endregion
    }
}
