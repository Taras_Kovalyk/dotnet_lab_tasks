﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class Program
    {
        static void Main(string[] args)
        {

            double[,] a = { { 3.5,  -2,  1,   2 },
                            { 2,     4, -5,   4 },
                            { 4.2,   4,  1.2, 7 },
                            { 3,    -5,  4, 1 } };

            var matrix1 = new Matrix(a);


            var matrix2 = new Matrix(4,4);

            for (var i = 0; i < matrix2.RowCount; i++)
            {
                for (var j = 0; j < matrix2.ColumnCount; j++)
                {
                    matrix2[i, j] = (i + 1) * (j + 1);
                }
            }

            Console.WriteLine("Matrix1: \n" + matrix1);
            Console.WriteLine();
            Console.WriteLine("Matrix2: \n" + matrix2);
            Console.WriteLine();


            var addMatrix = matrix1 + matrix2;
            var subMatrix = matrix1 - matrix2;
            var mulMatrix = matrix1 * matrix2;


            Console.WriteLine("\n" + new string('-',50) + "\n");
            Console.WriteLine("Matrix1 + Matrix2: \n" + addMatrix);
            Console.WriteLine();
            Console.WriteLine("Matrix1 - Matrix2: \n" + subMatrix);
            Console.WriteLine();
            Console.WriteLine("Matrix1 * Matrix2: \n" + mulMatrix);
            Console.WriteLine("\n" + new string('-', 50) + "\n");


            Console.WriteLine("Minor M(2,2) of Matrix1: " + matrix1.GetMinor(2, 2));
            Console.WriteLine();

            int[] rows = { 0, 1 };
            int[] cols = { 2, 3 };


            Console.WriteLine("Minor of k-th order (selected rows: 0, 1; selected columns: 2, 3): " + matrix1.GetMinorOfKthOrder(rows, cols));
            Console.WriteLine();

            Console.WriteLine("Additional minor (selected rows: 0, 1; selected columns: 2, 3): " + matrix1.GetAdditionalMinor(rows, cols));
            Console.WriteLine("\n" + new string('-', 50) + "\n");


            Console.ReadLine();
        }
    }
}
